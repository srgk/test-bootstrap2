auth_checker = function () {
  // redirect to login if not authorized

  let auth_token = localStorage.getItem("auth_token");

  if (auth_token === null) {
      window.alert("Not authorized");

      localStorage.setItem("redirected_from", location.href);

      window.location.replace("./login.html");
  }

};

auth_checker();
