/*
 An entry point for whole everpl client code
 */

fill_url_if_present = function () {
  let url = localStorage.getItem("base_url");

  let form_elements = document.getElementById("login_form");
  let base_url_field = form_elements[0];

  base_url_field.value = url;
};

process_auth = function () {
    // send a form data to the server and parse its response

    let form_elements = document.getElementById("login_form");

    let base_url = form_elements[0].value;
    let username = form_elements[1].value;
    let password = form_elements[2].value;

    if (base_url.length === 0) {
        alert("Empty base url");
    }

    if (username.length === 0) {
        alert("Empty username");
    }

    if (password.length === 0) {
        alert("Empty password");
    }

    localStorage.setItem("base_url", base_url);

    let request = new XMLHttpRequest();

    request.open('POST', new URL('auth', base_url), true);
    request.setRequestHeader('Content-Type', 'application/json');

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                let response = JSON.parse(request.responseText);
                let token = response.token;
                localStorage.setItem("auth_token", token);

                let destination = localStorage.getItem("redirected_from") || "./index.html";

                window.location.replace(destination);
            }
            else {
                alert(request.responseText);
            }
        }
    };

    request.send(JSON.stringify(
        {
            "username": username,
            "password": password
        }
    ));
};


get_placements = function () {
    let auth_token = localStorage.getItem("auth_token");
    let base_url = localStorage.getItem("base_url");

    let request = new XMLHttpRequest();

    request.open('GET', new URL('placements/', base_url), false);
    request.setRequestHeader('Authorization', auth_token);

    let response = null;

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                response = JSON.parse(request.responseText);
            }
            else {
                alert(request.responseText);

                response = JSON.parse(request.responseText);

                if (response.error_id === 2101) {
                    localStorage.removeItem("auth_token");
                    auth_checker();
                }
            }
        }
    };

    request.send();

    return response;
};

update_placements = function () {
    let response = get_placements();
    let placements = response.placements;

    localStorage.setItem("cached_placements", JSON.stringify(placements));
};

fill_placement_boxes = function () {
    let placements = JSON.parse(localStorage.getItem("cached_placements"));

    let container = document.querySelector("#placements");

    let t = document.querySelector('#placement-template');

    let name_holder = t.content.querySelector('.box-title');
    let image_holder = t.content.querySelector('.img-container');
    let link_holder = t.content.querySelector('a');

    for (let i = 0; i < placements.length; i++) {
        let item = placements[i];

        name_holder.textContent = item.friendly_name;
        image_holder.style.backgroundImage = "url('" + item.image_url + "')";
        link_holder.href = "./placement_page.html#" + item.id;

        let clone = document.importNode(t.content, true);
        container.appendChild(clone);
    }
};

fill_placement_menu = function (is_on_placement_page = false) {
    let placements = JSON.parse(localStorage.getItem("cached_placements"));

    let container = document.querySelector("#placements-menu");

    let t = document.querySelector('#placement-menu-template');

    let link_holder = t.content.querySelector('a');

    let current_placement_id = null;

    if (is_on_placement_page) {
        current_placement_id = get_placement_id();
    }

    for (let i = 0; i < placements.length; i++) {
        let item = placements[i];

        link_holder.href = "./placement_page.html#" + item.id;
        link_holder.textContent = item.friendly_name;

        let clone = document.importNode(t.content, true);

        if (current_placement_id === item.id) {
            clone.querySelector('li').classList.add('active');
        }

        container.appendChild(clone);
    }
};

fill_placement_info = function () {
    let identifier = get_placement_id();
    validate_placement_id(identifier);

    let info = get_placement_info(identifier);

    let container = document.querySelector(".content-header");

    let t = document.querySelector('#placement-info-template');

    let placement_name = info.friendly_name;
    let title_holder = t.content.querySelector('h1');

    title_holder.textContent = placement_name;

    let clone = document.importNode(t.content, true);
    container.appendChild(clone);
};

get_placement_id = function () {
    return location.hash.substring(1);
};

validate_placement_id = function (identifier) {
    if (identifier === null || identifier === "") {
        alert("Placement ID is not specified");

        window.location.replace('./index.html');
    }
};

get_placement_info = function (identifier) {
    let auth_token = localStorage.getItem("auth_token");
    let base_url = localStorage.getItem("base_url");

    let request = new XMLHttpRequest();

    request.open('GET', new URL('placements/' + identifier, base_url), false);
    request.setRequestHeader('Authorization', auth_token);

    let response = null;

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                response = JSON.parse(request.responseText);
            }
            else {
                alert(request.responseText);

                response = JSON.parse(request.responseText);

                if (response.error_id === 2101) {
                    localStorage.removeItem("auth_token");
                    auth_checker();
                }
            }
        }
    };

    request.send();

    return response;
};

get_things = function (in_placement = null) {
    let auth_token = localStorage.getItem("auth_token");
    let base_url = localStorage.getItem("base_url");

    let request = new XMLHttpRequest();

    let target_url = null;

    if (in_placement !== null) {
        target_url = new URL('things/?placement=' + in_placement, base_url);
    }
    else {
        target_url = new URL('things/', base_url);
    }

    request.open('GET', target_url, false);
    request.setRequestHeader('Authorization', auth_token);

    let response = null;

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                response = JSON.parse(request.responseText);
            }
            else {
                alert(request.responseText);

                response = JSON.parse(request.responseText);

                if (response.error_id === 2101) {
                    localStorage.removeItem("auth_token");
                    auth_checker();
                }
            }
        }
    };

    request.send();

    return response;
};

fill_things = function (in_placement = false) {
    let things = null;

    if (in_placement) {
        let placement = get_placement_id();
        things = get_things(placement).things;
    }
    else {
        things = get_things().things;
    }

    let container = document.querySelector("#things");

    let t = document.querySelector('#controllable-template');

    let thing_box = t.content.querySelector('.box');
    let name_holder = t.content.querySelector('.box-title');
    let toggle_btn = t.content.querySelector('.toggle-button');
    let toggle_btn_label = t.content.querySelector('label');



    for (let i = 0; i < things.length; i++) {
        let item = things[i];

        let thing_box_id = "th-" + item.id + "-box";
        let thing_btn_id = "th-" + item.id + "-btn";

        thing_box.id = thing_box_id;
        toggle_btn.id = thing_btn_id;
        toggle_btn_label.htmlFor = thing_btn_id;

        name_holder.textContent = item.friendly_name || item.type + " " + item.id;

        toggle_btn.checked = item.is_active;
        toggle_btn.disabled = ! item.is_available;

        let clone = document.importNode(t.content, true);
        let toggle_btn_clone = clone.querySelector('.toggle-button');
        toggle_btn_clone.addEventListener("click", button_click_handler);
        container.appendChild(clone);
    }
};

get_thing_info = function (thing_id) {
    let auth_token = localStorage.getItem("auth_token");
    let base_url = localStorage.getItem("base_url");

    let request = new XMLHttpRequest();

    request.open('GET', new URL('things/' + thing_id, base_url), false);
    request.setRequestHeader('Authorization', auth_token);

    let response = null;

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status === 200) {
                response = JSON.parse(request.responseText);
            }
            else {
                alert(request.responseText);

                response = JSON.parse(request.responseText);

                if (response.error_id === 2101) {
                    localStorage.removeItem("auth_token");
                    auth_checker();
                }
            }
        }
    };

    request.send();

    return response;
};

send_actuator_command = function (identifier, command, params, callback) {
    let auth_token = localStorage.getItem("auth_token");
    let base_url = localStorage.getItem("base_url");
    let target_url = new URL("things/" + identifier + "/execute", base_url);

    let request = new XMLHttpRequest();

    request.open('POST', target_url.toString(), true);
    request.setRequestHeader('Authorization', auth_token);
    request.setRequestHeader('Content-Type', 'application/json');

    let response = null;

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status !== 202) {
                alert(request.responseText);

                response = JSON.parse(request.responseText);

                if (response.error_id === 2101) {
                    localStorage.removeItem("auth_token");
                    auth_checker();
                }
            }

            callback(response);
        }
    };

    request.send(JSON.stringify(
        {
	        "command": command,
            "command_args": params
        }
    ));
};

button_click_handler = function (event) {
    //event.preventDefault();

    let toggle_btn = event.target;
    let btn_id = toggle_btn.id;
    let thing_id = btn_id.substring(3, btn_id.length - 4);

    toggle_btn.disabled = true;

    let command = null;

    if (toggle_btn.checked) {
        command = "activate";
    }

    else {
        command = "deactivate";
    }

    let finish_actuator_command = function(response) {
        let thing_info = get_thing_info(thing_id);

        toggle_btn.checked = thing_info.is_active;
        toggle_btn.disabled = ! thing_info.is_available;
    };

    send_actuator_command(thing_id, command, {}, finish_actuator_command);
};

sign_out = function () {
    localStorage.removeItem("auth_token");
    auth_checker();
};